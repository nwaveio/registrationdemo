package io.nwave.pkudinov.registrationdemo.server_api.interfaces;

/**
 * Created by Pavel Kudinov on 20.06.2018.
 */
@FunctionalInterface
public interface ICancelable {
    void cancel();
}
