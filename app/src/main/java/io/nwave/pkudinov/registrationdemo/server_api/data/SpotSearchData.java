package io.nwave.pkudinov.registrationdemo.server_api.data;

/**
 * Created by Pavel Kudinov on 17.07.2018.
 */
public final class SpotSearchData {

    public String stop_time; // string. datetime format (YYYY-mm-dd HH:MM:ss). optional parameter, time when vehicle stopped
    public Geo geo;
    public int scan_duration;   // duration (in ms) since starting of scanning advertisings to time when current request is sent
    public Advertising_packet[] advertising_packets;

    static public final class Geo {
        public double latitude;
        public double longitude;
        public int accuracy; //integer, accuracy in meters
    }

    static public final class Advertising_packet {
        public String msd; //string. hex_encoded manufacture specific data
        public int rssi;
    }
}
