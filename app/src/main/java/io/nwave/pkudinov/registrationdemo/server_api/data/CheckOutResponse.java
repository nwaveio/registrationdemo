package io.nwave.pkudinov.registrationdemo.server_api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pavel Kudinov on 18.06.2018.
 */
public final class CheckOutResponse {
    public boolean success;
}
