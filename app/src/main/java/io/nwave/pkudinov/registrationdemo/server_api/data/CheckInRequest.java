package io.nwave.pkudinov.registrationdemo.server_api.data;

/**
 * Created by Pavel Kudinov on 07.08.2018.
 */
public final class CheckInRequest {
    public String sssid;
    public String sensor_id;
    public String user_id;
    public String vehicle_id;
    public String time; //YYYY-MM-dd HH:mm:ss
}
