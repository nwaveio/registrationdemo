package io.nwave.pkudinov.registrationdemo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Pavel Kudinov on 13.03.2018.
 */

public class UserPreferences {
    private static final String PREFS_NAME = "user";
    private static final String PARAM_STRING_USER_ID = "id";
    private static final String PARAM_STRING_VEHICLE_ID = "vehicle_id";
    private static final String PARAM_STRING_SERVER_ADDRESS = "server";

    private SharedPreferences preferences;

    public UserPreferences(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public String getUserId() {
        return preferences.getString(PARAM_STRING_USER_ID, null);
    }
    public void setUserId(String s) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PARAM_STRING_USER_ID, s);
        editor.apply();
    }

    public String getVehicleId() {
        return preferences.getString(PARAM_STRING_VEHICLE_ID, "Lada Kalina Sport");
    }
    public void setVehicleId(String s) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PARAM_STRING_VEHICLE_ID, s);
        editor.apply();
    }

    public String getServerAddress() {
        return preferences.getString(PARAM_STRING_SERVER_ADDRESS, "https://checkin.nwave.io");
    }
    public void setServerAddress(String s) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PARAM_STRING_SERVER_ADDRESS, s);
        editor.apply();
    }
}
