package io.nwave.pkudinov.registrationdemo.server_api.data;

/**
 * Created by Pavel Kudinov on 07.08.2018.
 */
public final class CheckOutRequest {
    public String time; //YYYY-MM-dd HH:mm:ss
}
