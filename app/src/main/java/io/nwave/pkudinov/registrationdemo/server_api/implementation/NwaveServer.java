package io.nwave.pkudinov.registrationdemo.server_api.implementation;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.UserPreferences;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchData;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchResponse;
import io.nwave.pkudinov.registrationdemo.server_api.data.StatusResponse;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.ICancelable;
import io.nwave.pkudinov.registrationdemo.server_api.INwaveServer;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnErrorResponseListener;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnResponseListener;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInResponse;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutResponse;

/**
 * Created by Pavel Kudinov on 15.06.2018.
 */
public class NwaveServer implements INwaveServer {

    //private static String URL = "http://192.168.88.195:1490";
    //private static String URL = "https://checkin.nwave.io";
    private static String URL;
    private static String token = "Thzxjkp6h0vwfJdmMf28S8oVwq7XInhv";

    public NwaveServer(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonRequest.setRequestQueue(queue);
        URL = new UserPreferences(context).getServerAddress();
    }

    public static void setURL(String URL) {
        NwaveServer.URL = URL;
    }

    @Override
    public CheckInResponse checkIn_blocking(CheckInRequest checkInRequest, String csid) throws InterruptedException, ExecutionException {
        return new JsonRequest<>(CheckInResponse.class).execute_blocking(Request.Method.POST, URL + "/api/v2/check_in_session/" + csid + "/start", checkInRequest, token);
    }

    @Override
    public ICancelable spotSearchRequest(SpotSearchData sssData, String sssId, IOnResponseListener<SpotSearchResponse> listener, IOnErrorResponseListener errorListener) {
        return new JsonRequest<>(SpotSearchResponse.class).execute(Request.Method.POST, URL + "/api/v2/spot_search/search/" + sssId, sssData, listener, errorListener, token);
    }

    @Override
    public CheckOutResponse checkOut_blocking(CheckOutRequest checkOutRequest, String csid) throws InterruptedException, ExecutionException {
        return new JsonRequest<>(CheckOutResponse.class).execute_blocking(Request.Method.POST, URL + "/api/v2/check_in_session/" + csid + "/end", checkOutRequest, token);
    }

    @Override
    public StatusResponse getStatus_blocking(String csid) throws InterruptedException, ExecutionException {
        return new JsonRequest<>(StatusResponse.class).execute_blocking(Request.Method.GET, URL + "/api/v2/check_in_session/" + csid, null, token);
    }
}
