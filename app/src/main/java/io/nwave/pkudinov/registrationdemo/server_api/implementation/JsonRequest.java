package io.nwave.pkudinov.registrationdemo.server_api.implementation;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.server_api.interfaces.ICancelable;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnErrorResponseListener;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnResponseListener;

/**
 * Created by Pavel Kudinov on 15.06.2018.
 */
class JsonRequest<TRequest, TResponse> {

    private static RequestQueue queue;
    private final Class<TResponse> responseClass;

    JsonRequest(Class<TResponse> responseClass) {
        this.responseClass = responseClass;
    }

    static void setRequestQueue(RequestQueue queue) {
        JsonRequest.queue = queue;
    }

    @SuppressWarnings("SameParameterValue")
    TResponse execute_blocking(int method, String url, final TRequest myRequest, final String token) throws ExecutionException, InterruptedException {
        final String body;
        if (myRequest == null) {
            body = null;
        } else {
            body = new GsonBuilder().create().toJson(myRequest);
        }
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest stringRequest = new StringRequest(method, url, future, future) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/json");
                if (token != null) {
                    params.put("X-Auth-Token", token);
                }
                return params;
            }
            @Override
            public byte[] getBody() {
                if (myRequest == null) return null;
                return body.getBytes(StandardCharsets.UTF_8);
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
        try {
            String response = future.get();
            try {
                return new GsonBuilder().registerTypeAdapter(responseClass, new AnnotatedDeserializer<TResponse>()).create().fromJson(response, responseClass);
            } catch (JsonSyntaxException ex) {
                throw new ExecutionException("wrong response format: " + response + " " + ex.getMessage(), ex);
            }
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof VolleyError) {
                throw new ExecutionException(getErrorMessage((VolleyError) ex.getCause(), url, body), ex.getCause());
            } else {
                throw ex;
            }
        }
    }

    @SuppressWarnings("SameParameterValue")
    ICancelable execute(int method, final String url, final TRequest myRequest, final IOnResponseListener<TResponse> listener, final IOnErrorResponseListener errorListener, final String token){
        final String body;
        if (myRequest == null) {
            body = null;
        } else {
            body = new GsonBuilder().create().toJson(myRequest);
        }
        final StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    TResponse obj = new GsonBuilder().registerTypeAdapter(responseClass, new AnnotatedDeserializer<TResponse>()).create().fromJson(response, responseClass);
                    listener.onResponse(obj);
                } catch (JsonParseException ex) {
                    errorListener.onErrorResponse(new ExecutionException("wrong response format: " + response + " " + ex.getMessage(), ex));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorListener.onErrorResponse(new ExecutionException(getErrorMessage(error, url, body), error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/json");
                if (token != null) {
                    params.put("X-Auth-Token", token);
                }
                return params;
            }
            @Override
            public byte[] getBody() {
                if (myRequest == null) return null;
                return body.getBytes(StandardCharsets.UTF_8);
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
        return new ICancelable() {
            @Override
            public void cancel() {
                stringRequest.cancel();
            }
        };
    }

    @SuppressWarnings("WeakerAccess")
    protected String getErrorMessage(VolleyError error, String url, String body) {
        return volleyErrorToString(error, url, body);
    }

    private static String volleyErrorToString(VolleyError error, String url, String body) {
        StringBuilder sb = new StringBuilder();
        if (error.networkResponse != null) {
            sb.append("Unexpected server response ").append(error.networkResponse.statusCode).append(" for ").append(url);
            if (body != null) {
                sb.append("; body ").append(body);
            }
            sb.append("; response ").append(new String(error.networkResponse.data));
        } else if (error instanceof TimeoutError) {
            sb.append("Timeout error on ").append(url);
        } else {
            sb.append(error.getMessage());
        }
        return sb.toString();
    }
}
