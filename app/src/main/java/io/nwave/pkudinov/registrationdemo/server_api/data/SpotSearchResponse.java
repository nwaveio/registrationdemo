package io.nwave.pkudinov.registrationdemo.server_api.data;

import com.google.gson.annotations.SerializedName;

import io.nwave.pkudinov.registrationdemo.server_api.interfaces.FieldRequired;

/**
 * Created by Pavel Kudinov on 18.07.2018.
 */
public final class SpotSearchResponse {

    @FieldRequired public ESatus status;
    public String sssid;
    public String[] valid_sensors;
    public Available_sensor[] available_sensors;

    public enum ESatus {
        @SerializedName("completed") COMPLETED,
        @SerializedName("continue") CONTINUE,
        @SerializedName("timeout") TIMEOUT,
        @SerializedName("duplicate") DUPLICATE
    }

    public final static class Available_sensor {
        public String sensor_id;    // sensor_id is hex string
        public Integer probability; // unsigned integer from 0 to 100
        public Boolean engaged;
        public String engaged_time; //YYYY-MM-dd HH:mm:ss
    }
}
