package io.nwave.pkudinov.registrationdemo.bluetooth;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ScanTask {

    private BluetoothAdapter mBluetoothAdapter;
    private ScannerApi scannerApi;

    private interface ScannerApi {
        void startScanning();
        void stopScanning();
    }

    public static class BleAdvDevice {
        public final BluetoothDevice device;
        public final int rssi;
        public final byte[] scanRecord;

        private BleAdvDevice(BluetoothDevice device, int rssi, byte[] scanRecord) {
            this.device = device;
            this.rssi = rssi;
            this.scanRecord = scanRecord;
        }
    }

    private final ReentrantLock lock = new ReentrantLock();
    private List<BleAdvDevice> bleAdvDeviceList = new ArrayList<>();

    public List<BleAdvDevice> getBleAdvDevices() {
        lock.lock();
        List<BleAdvDevice> ret = new ArrayList<>(bleAdvDeviceList);
        bleAdvDeviceList.clear();
        lock.unlock();
        return ret;
    }

    private void clearBleAdvDeviceList() {
        lock.lock();
        bleAdvDeviceList.clear();
        lock.unlock();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            lock.lock();
            bleAdvDeviceList.add(new BleAdvDevice(device, rssi, scanRecord));
            lock.unlock();
        }
    };


    @SuppressLint("ObsoleteSdkInt")
    public ScanTask(BluetoothAdapter bluetoothAdapter) {
        this.mBluetoothAdapter = bluetoothAdapter;

        ScannerApi scannerApi18 = new ScannerApi() {
            @SuppressWarnings("deprecation")
            @Override
            public void startScanning() {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }

            @SuppressWarnings("deprecation")
            @Override
            public void stopScanning() {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        };

        ScannerApi scannerApi21 = new ScannerApi() {
            BluetoothLeScanner scanner;
            ScanCallback callback;
            ScanSettings settings;
            @TargetApi(21)
            @Override
            public void startScanning() {
                if (scanner == null) {
                    scanner = mBluetoothAdapter.getBluetoothLeScanner();
                    if (Build.VERSION.SDK_INT >= 23) {
                        settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(0).setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE).build();
                    } else {
                        settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(0).build();
                    }
                    callback = new ScanCallback() {
                        @Override
                        public void onScanResult(int callbackType, ScanResult result) {
                            if (result != null && result.getScanRecord() != null)
                                mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
                        }

                        @Override
                        public void onBatchScanResults(List<ScanResult> results) {
                            for (ScanResult result : results)
                                if (result != null && result.getScanRecord() != null)
                                    mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
                        }
                    };
                }
                scanner.startScan(null, settings, callback);
            }

            @TargetApi(21)
            @Override
            public void stopScanning() {
                if (scanner != null)
                    scanner.stopScan(callback);
            }
        };

        scannerApi = Build.VERSION.SDK_INT < 21 ? scannerApi18 : scannerApi21;
    }

    public void startScanning() {
        clearBleAdvDeviceList();
        scannerApi.startScanning();
    }
    public void stopScanning() {
        scannerApi.stopScanning();
    }
}
