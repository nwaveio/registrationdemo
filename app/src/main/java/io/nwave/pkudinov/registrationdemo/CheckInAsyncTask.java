package io.nwave.pkudinov.registrationdemo;

import android.app.AlertDialog;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInResponse;

/**
 * Created by Pavel Kudinov on 21.06.2018.
 */
class CheckInAsyncTask extends AsyncTask<String, String, Boolean> {
    private WeakReference<MainActivity> mainActivity;

    CheckInAsyncTask(MainActivity mainActivity) {
        this.mainActivity = new WeakReference<>(mainActivity);
    }


    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            String csid = strings[0];

            CheckInRequest checkInRequest = new CheckInRequest();
            checkInRequest.sssid = strings[1];
            checkInRequest.sensor_id = strings[2];
            checkInRequest.user_id = strings[3];
            checkInRequest.vehicle_id = strings[4];

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            checkInRequest.time = sdf.format(new Date());

            CheckInResponse response = mainActivity.get().iNwaveServer.checkIn_blocking(checkInRequest, csid);
            if (response.success) {
                publishProgress("Check in successful", "#009933");
                return true;
            }
            return false;
        } catch (ExecutionException ex) {
            ex.printStackTrace();
            publishProgress(ex.getMessage(), "red");
            return false;
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return false;
        }
    }



    @Override
    protected void onPreExecute() {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            mainActivity.get().checkInButton.setEnabled(false);
            mainActivity.get().printLn("Check in started");
        }
    }

    @Override
    protected void onProgressUpdate(String... strings) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            mainActivity.get().printLn(strings);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            new AlertDialog.Builder(mainActivity.get())
                    .setMessage(result ? R.string.check_in_done : R.string.check_in_fail)
                    .show();
            mainActivity.get().checkOutButton.setEnabled(result);
            mainActivity.get().checkInButton.setEnabled(!result);
        }
    }
}
