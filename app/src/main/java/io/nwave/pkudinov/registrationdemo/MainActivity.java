package io.nwave.pkudinov.registrationdemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.nwave.pkudinov.registrationdemo.bluetooth.ScanTask;
import io.nwave.pkudinov.registrationdemo.server_api.INwaveServer;
import io.nwave.pkudinov.registrationdemo.server_api.data.StatusResponse;
import io.nwave.pkudinov.registrationdemo.server_api.implementation.NwaveServer;

public class MainActivity extends AppCompatActivity {

    private final static int RC_ENABLE_BLUETOOTH = 10;
    private final static int RC_ENABLE_LOCATION = 11;
    private final static int RC_PERMISSION_LOCATION = 12;

    Button checkInButton;
    Button checkOutButton;
    Button spotSearchButton;

    private ScrollView scrollView;
    private TextView console;
    private View space;
    String sssid;
    String csid;
    String available_sensor;
    private String checkOutedCsid = "";
    private final static Object csidMutex = new Object();

    private UserPreferences userPreferences;

    private BluetoothAdapter mBluetoothAdapter;
    ScanTask scanTask;
    INwaveServer iNwaveServer;

    private FusedLocationProviderClient mFusedLocationClient;
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkInButton = findViewById(R.id.button_checkIn);
        checkOutButton = findViewById(R.id.button_checkOut);
        spotSearchButton = findViewById(R.id.button_spotSearch);

        console = findViewById(R.id.textView_console);
        scrollView = findViewById(R.id.scroll_view);
        space = findViewById(R.id.space);

        userPreferences = new UserPreferences(this);
        iNwaveServer = new NwaveServer(this);

        // Initialize Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null || !getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            // Device does not support Bluetooth Low Energy
            Toast.makeText(getApplicationContext(), "Bluetooth Low Energy is not supported on this device", Toast.LENGTH_LONG).show();
            finish();
        }
        scanTask = new ScanTask(mBluetoothAdapter);

        //if (Build.VERSION.SDK_INT >= 23) {
            requestLocationPermission();
        //}

        checkInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (csidMutex) {
                    csid = "csid" + ((int) (Math.random() * 100000));
                }
                printLn(csid + " sensor " + available_sensor);
                new CheckInAsyncTask(MainActivity.this).execute(csid, sssid, available_sensor, userPreferences.getUserId(), userPreferences.getVehicleId());
            }
        });

        checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CheckOutAsyncTask(MainActivity.this).execute(csid);
                synchronized (csidMutex) {
                    csid = null;
                }
            }
        });

        spotSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBluetoothAdapter.isEnabled()) {
                    if (checkLocationServices()) {
                        new SpotSearchAsyncTask(MainActivity.this).execute();
                    } else {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle(R.string.location_services_disabled)
                                .setMessage(R.string.explain_location_request)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), RC_ENABLE_LOCATION);
                                    }
                                })
                                .show();
                    }
                } else {
                    Intent enableBtIntent = new Intent (BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, RC_ENABLE_BLUETOOTH);
                }
            }
        });

        printLn("Server: " + userPreferences.getServerAddress());
        printLn("Vehicle ID: " + userPreferences.getVehicleId());
        if (userPreferences.getUserId() == null) {
            showSetUserIdDialog();
        } else {
            printLn("User ID: " + userPreferences.getUserId());
        }

        // Only for demo app
        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                final String csid1;
                synchronized (csidMutex) {
                    if (csid == null || csid.equals(checkOutedCsid)) {
                        return;
                    }
                    csid1 = csid;
                }
                try {
                    StatusResponse status = iNwaveServer.getStatus_blocking(csid1);
                    synchronized (csidMutex) {
                        if (status.possibly_complete && status.id.equals(csid)) {
                            checkOutedCsid = csid;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    printLn("Session " + checkOutedCsid + " release notification", "#5B00CF");
                                }
                            });
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 1, 1, TimeUnit.SECONDS);

        registerLocationListeners();
    }

    @Override
    protected void onDestroy() {
        executorService.shutdownNow();
        super.onDestroy();
    }

    @TargetApi(23)
    void requestLocationPermission() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setTitle(R.string.permission_request)
                        .setMessage(R.string.explain_location_request)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_PERMISSION_LOCATION);
                            }
                        })
                        .show();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_PERMISSION_LOCATION);
            }
        }
    }

    void printLn(String... message) {
        String res = message[0];
        if (message.length > 1) {
            res = "<font color=" + message[1] + ">" + res + "</font>";
        }
        console.append(Html.fromHtml(res));
        console.append("\n");

        Rect scrollBounds = new Rect();
        scrollView.getHitRect(scrollBounds);
        if (space.getLocalVisibleRect(scrollBounds)) {
            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
        }
    }

    private void showSetUserIdDialog() {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.dialog_user_id, null);
        final EditText userIdEditText = view.findViewById(R.id.editText_userId);
        userIdEditText.setText(userPreferences.getUserId());
        checkOutButton.setEnabled(false);
        checkInButton.setEnabled(false);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.set_user_id)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userPreferences.setUserId(userIdEditText.getText().toString());
                        printLn("User ID: " + userIdEditText.getText().toString());
                    }
                })
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface di) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(userIdEditText.getText().length() != 0);
            }
        });

        userIdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialog.setView(view);
        dialog.show();
    }

    private void showSetVehicleIdDialog() {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.dialog_user_id, null);
        final EditText userIdEditText = view.findViewById(R.id.editText_userId);
        userIdEditText.setText(userPreferences.getVehicleId());
        checkOutButton.setEnabled(false);
        checkInButton.setEnabled(false);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Set vehicle ID")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userPreferences.setVehicleId(userIdEditText.getText().toString());
                        printLn("Vehicle ID: " + userIdEditText.getText().toString());
                    }
                })
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface di) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(userIdEditText.getText().length() != 0);
            }
        });

        userIdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialog.setView(view);
        dialog.show();
    }

    private void showSetServerAddressDialog() {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.dialog_user_id, null);
        final EditText userIdEditText = view.findViewById(R.id.editText_userId);
        userIdEditText.setText(userPreferences.getServerAddress());
        checkOutButton.setEnabled(false);
        checkInButton.setEnabled(false);
        userIdEditText.setHint("https://checkin.nwave.io");

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Set server address")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userPreferences.setServerAddress(userIdEditText.getText().toString());
                        printLn("Server: " + userIdEditText.getText().toString());
                        NwaveServer.setURL(userIdEditText.getText().toString());
                    }
                })
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface di) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(userIdEditText.getText().length() != 0);
            }
        });

        userIdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(s.length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialog.setView(view);
        dialog.show();
    }

    private boolean checkLocationServices() {
        return Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF) != Settings.Secure.LOCATION_MODE_OFF;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == RC_PERMISSION_LOCATION) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.pernission_denied)
                        .setMessage(R.string.permission_denied_message)
                        .setCancelable(false)
                        .setPositiveButton ("EXIT", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .show();
            } else {
                registerLocationListeners();
                startLocationUpdates();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                spotSearchButton.callOnClick();
            }
        } else if (requestCode == RC_ENABLE_LOCATION) {
            if (checkLocationServices()) {
                spotSearchButton.callOnClick();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_userId:
                showSetUserIdDialog();
                return true;
            case R.id.menu_vehicleId:
                showSetVehicleIdDialog();
                return true;
            case R.id.menu_serverAddress:
                showSetServerAddressDialog();
                return true;
            case R.id.menu_version:
                StringBuilder sb = new StringBuilder();
                try {
                    PackageInfo packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
                    sb.append("Version code: ");
                    sb.append(Integer.toString(packageInfo.versionCode));
                    sb.append("\n");
                    sb.append(packageInfo.versionName);
                    sb.append("\n");
                    sb.append(new Date(BuildConfig.TIMESTAMP).toString());
                    new AlertDialog.Builder(this).setMessage(sb.toString()).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    static final Object locationMutex = new Object();
    Location bestLocation;

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private void registerLocationListeners() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            synchronized (locationMutex) {
                                if (isBetterLocation(location, bestLocation)) {
                                    bestLocation = location;
                                    //printLn("accuracy: " + (int)location.getAccuracy(), "black");
                                }
                            }
                        }
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    private LocationCallback mLocationCallback;
    private void startLocationUpdates() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    synchronized (locationMutex) {
                        for (Location location : locationResult.getLocations()) {
                            // Update UI with location data
                            // ...
                            //printLn("accuracy: " + (int)location.getAccuracy() + " src: " + location.getProvider(), "black");
                            if (isBetterLocation(location, bestLocation)) {
                                bestLocation = location;
                            }
                        }
                    }
                };
            };

            mFusedLocationClient.requestLocationUpdates(createLocationRequest(),
                    mLocationCallback,
                    null /* Looper */);
        }
    }

    private LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }
}
