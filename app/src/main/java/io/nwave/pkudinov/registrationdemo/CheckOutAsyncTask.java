package io.nwave.pkudinov.registrationdemo;

import android.app.AlertDialog;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutResponse;

/**
 * Created by Pavel Kudinov on 16.06.2018.
 */
class CheckOutAsyncTask extends AsyncTask<String, String, Boolean> {
    private WeakReference<MainActivity> mainActivity;

    CheckOutAsyncTask(MainActivity mainActivity) {
        this.mainActivity = new WeakReference<>(mainActivity);
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            String csid = strings[0];
            CheckOutRequest checkOutRequest = new CheckOutRequest();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            checkOutRequest.time = sdf.format(new Date());

            CheckOutResponse response = mainActivity.get().iNwaveServer.checkOut_blocking(checkOutRequest, csid);
            if (response.success) {
                publishProgress("Check out successful", "#009933");
                return true;
            }
            return false;
        } catch (ExecutionException ex) {
            ex.printStackTrace();
            publishProgress(ex.getMessage(), "red");
            return false;
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            mainActivity.get().checkOutButton.setEnabled(false);
            mainActivity.get().printLn("Check out started");
        }
    }

    @Override
    protected void onProgressUpdate(String... strings) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            mainActivity.get().printLn(strings);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            new AlertDialog.Builder(mainActivity.get())
                    .setMessage(result ? R.string.check_out_done : R.string.check_out_fail)
                    .show();
            mainActivity.get().checkOutButton.setEnabled(!result);
        }
    }
}
