package io.nwave.pkudinov.registrationdemo.server_api;

import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutRequest;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchData;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckInResponse;
import io.nwave.pkudinov.registrationdemo.server_api.data.CheckOutResponse;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchResponse;
import io.nwave.pkudinov.registrationdemo.server_api.data.StatusResponse;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.ICancelable;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnErrorResponseListener;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnResponseListener;

/**
 * Created by Pavel Kudinov on 15.06.2018.
 */
public interface INwaveServer {

    CheckInResponse checkIn_blocking(CheckInRequest checkInRequest, String csid) throws InterruptedException, ExecutionException;

    ICancelable spotSearchRequest(SpotSearchData event, String sssId, IOnResponseListener<SpotSearchResponse> listener, IOnErrorResponseListener errorListener);

    CheckOutResponse checkOut_blocking(CheckOutRequest checkInRequest, String csid) throws InterruptedException, ExecutionException;

    StatusResponse getStatus_blocking(String csid) throws InterruptedException, ExecutionException;
}
