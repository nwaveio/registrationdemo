package io.nwave.pkudinov.registrationdemo;

import android.app.AlertDialog;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import io.nwave.pkudinov.registrationdemo.async.SpotSearchAsync;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchData;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchResponse;

/**
 * Created by Pavel Kudinov on 07.08.2018.
 */
public class SpotSearchAsyncTask extends SpotSearchAsync {

    private WeakReference<MainActivity> mainActivity;
    private String stopTime;
    private long timeStampStart;

    SpotSearchAsyncTask(MainActivity mainActivity) {
        super(mainActivity.scanTask, mainActivity.iNwaveServer);
        this.mainActivity = new WeakReference<>(mainActivity);

        long st = System.currentTimeMillis() - 10000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        stopTime = sdf.format(new Date(st));
    }

    @Override
    protected SpotSearchData.Geo getGeo() {
        SpotSearchData.Geo geo = new SpotSearchData.Geo();
        synchronized (MainActivity.locationMutex) {
            if (mainActivity.get().bestLocation != null) {
                geo.latitude = mainActivity.get().bestLocation.getLatitude();
                geo.longitude = mainActivity.get().bestLocation.getLongitude();
                geo.accuracy = (int) mainActivity.get().bestLocation.getAccuracy();
            }
        }
        return geo;
    }

    @Override
    protected String getStopTime() {
        return stopTime;
    }

    @Override
    protected void onPreExecute() {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            mainActivity.get().printLn("Spot search started");
            timeStampStart = System.currentTimeMillis();
            mainActivity.get().spotSearchButton.setEnabled(false);
            mainActivity.get().checkInButton.setEnabled(false);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {
            new AlertDialog.Builder(mainActivity.get())
                    .setMessage(result ? "Spot search done" : "Spot search fail")
                    .show();
            if (result) {
                mainActivity.get().sssid = sssId;
                mainActivity.get().available_sensor = available_sensors[0];
                mainActivity.get().checkInButton.setEnabled(true);
            }
            mainActivity.get().spotSearchButton.setEnabled(true);
        }
    }

    static private String arrayToString(String[] ar) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ar.length; ++i) {
            sb.append(ar[i]);
            if (i != ar.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    static private String availableSensorsToString(SpotSearchResponse.Available_sensor[] sensors) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sensors.length; ++i) {
            sb.append(sensors[i].sensor_id).append(" ").append(sensors[i].probability).append("%")
                    .append(sensors[i].engaged ? " Occupied" : " Free");
            if (sensors[i].engaged_time != null) {
                sb.append(" ").append(sensors[i].engaged_time);
            }
            if (i != sensors.length - 1) {
                sb.append(",<br>");
            }
        }
        return sb.toString();
    }

    @Override
    protected void onProgressUpdate(IActionMark... action) {
        if (mainActivity.get() != null && !mainActivity.get().isFinishing()) {

            if (action[0] instanceof ActionHttpResponse) {
                SpotSearchResponse data = ((ActionHttpResponse) action[0]).spotSearchResponse;
                switch (data.status) {
                    case COMPLETED:
                        mainActivity.get().printLn("Spot search successful in " + (System.currentTimeMillis() - timeStampStart) + " ms. Valid sensors: " + arrayToString(data.valid_sensors) + ". Available sensors:<br>" + availableSensorsToString(data.available_sensors), "#009933");
                        break;
                    case CONTINUE:
                        mainActivity.get().printLn("Keep advertising reception. Valid sensors: " + arrayToString(data.valid_sensors) + ".");
                        break;
                    case TIMEOUT:
                        mainActivity.get().printLn("Server: No sensors available", "red");
                        break;
                    case DUPLICATE:
                        mainActivity.get().printLn("duplicate response");
                        break;
                }

            } else if (action[0] instanceof ActionHttpError) {
                ExecutionException exception = ((ActionHttpError) action[0]).executionException;
                mainActivity.get().printLn(exception.getMessage(), "red");

            } else if (action[0] instanceof ActionAdvertisingEvents) {
                List<SensorInfo> list = ((ActionAdvertisingEvents) action[0]).sensorInfoList;
                mainActivity.get().printLn("Advertising: " + list.size() + ", geo accuracy: " + ((ActionAdvertisingEvents) action[0]).geo.accuracy + " m");

            } else if (action[0] instanceof ActionRestartScan) {
                mainActivity.get().printLn("Restart scan");

            } else if (action[0] instanceof ActionNoSensorsNearby) {
                mainActivity.get().printLn("No sensors nearby", "red");
            }
        }
    }
}
