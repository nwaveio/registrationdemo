package io.nwave.pkudinov.registrationdemo.server_api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pavel Kudinov on 16.06.2018.
 */
public final class AdvertisingEvent {

    public String sensor_id;
    public EStatus status;
    public String encoded_package;
    public int rssi;

    public enum EStatus {
        @SerializedName("free") FREE,
        @SerializedName("occupied") OCCUPIED,
        @SerializedName("initialization") INITIALIZATION
    }
}
