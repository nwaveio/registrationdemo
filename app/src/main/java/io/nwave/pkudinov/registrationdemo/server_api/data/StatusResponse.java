package io.nwave.pkudinov.registrationdemo.server_api.data;

import io.nwave.pkudinov.registrationdemo.server_api.interfaces.FieldRequired;

/**
 * Created by Pavel Kudinov on 18.06.2018.
 */
public final class StatusResponse {
    @FieldRequired
    public String id;
    public String parent_sssid;
    public String vehicle_id;
    public String start_time;
    public String end_time;
    public boolean possibly_complete;
    public String[] involved_sensors;
}
