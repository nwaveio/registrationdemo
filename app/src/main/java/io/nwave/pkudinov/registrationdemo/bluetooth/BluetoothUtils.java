package io.nwave.pkudinov.registrationdemo.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.Nullable;

import java.util.Arrays;

import io.nwave.pkudinov.registrationdemo.server_api.data.AdvertisingEvent;

/**
 * Created by Pavel Kudinov on 16.06.2018.
 */
@SuppressWarnings("WeakerAccess")
public class BluetoothUtils {

    private static final byte[] DEVICE_API_VERSION_MAGIC_WORD = { (byte)0xB1, (byte)0xB2, (byte)0xB3, (byte)0xC4 };

    private static final int MAGIC_WORD_OFFSET = 0;
    private static final int FLAGS_OFFSET = 4;
    private static final int ENCRYPTED_PACKAGE_OFFSET = 5;

    @Nullable
    public static byte[] getManufacturerData(byte[] data) {
        if (data == null)
            return null;
        int dataSize = data.length;
        int structOffset = 0;
        while (dataSize >= 3) {
            if (data[structOffset] == 0) {
                return null;
            }
            int structSize = data[structOffset] + 1;
            if (data[structOffset + 1] == (byte)0xFF) {
                return Arrays.copyOfRange(data, structOffset + 2, structOffset + structSize);
            }
            structOffset += structSize;
            dataSize -= structSize;
        }
        return null;
    }

    private static boolean isNwaveManufacturer(byte[] manufacturerData) {
        return manufacturerData != null
                && manufacturerData.length == 21
                && Arrays.equals(DEVICE_API_VERSION_MAGIC_WORD, Arrays.copyOfRange(manufacturerData, MAGIC_WORD_OFFSET, MAGIC_WORD_OFFSET + DEVICE_API_VERSION_MAGIC_WORD.length));
    }

    private static final byte DEVICE_API_VERSION_MAGIC_WORD_V2_BYTE1 = 0x57;
    private static final byte DEVICE_API_VERSION_MAGIC_WORD_V2_BYTE2 = 0x10;

    public static boolean isNwaveManufacturer_v2(byte[] manufacturerData) {
        return manufacturerData != null
                && manufacturerData[MAGIC_WORD_OFFSET] == DEVICE_API_VERSION_MAGIC_WORD_V2_BYTE1
                && (manufacturerData[MAGIC_WORD_OFFSET + 1] & 0xF0) == DEVICE_API_VERSION_MAGIC_WORD_V2_BYTE2;
    }

    private static String getNetworkId(BluetoothDevice device) {
        return device.getAddress().replaceAll(":","").substring(4).toUpperCase();
    }

    private static int getStatusCode(byte[] manufacturerData) {
        return manufacturerData[FLAGS_OFFSET] & 0b11;
    }

    private static String getEncryptedPackage(byte[] manufacturerData) {
        return bytesToHex(Arrays.copyOfRange(manufacturerData, ENCRYPTED_PACKAGE_OFFSET, ENCRYPTED_PACKAGE_OFFSET + 16));
    }

    private final static char[] _hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = _hexArray[v >>> 4];
            hexChars[j * 2 + 1] = _hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static class DeviceInfo {
        public String networkId;
        public int statusCode;
        public String encryptedPackage;
    }

    @Nullable
    public static DeviceInfo getDeviceInfo(BluetoothDevice device, byte[] advData) {
        byte[] manufacturerData = getManufacturerData(advData);
        if (isNwaveManufacturer(manufacturerData)) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.networkId = getNetworkId(device);
            deviceInfo.statusCode = getStatusCode(manufacturerData);
            deviceInfo.encryptedPackage = getEncryptedPackage(manufacturerData);
            return deviceInfo;
        }
        return null;
    }

    @Nullable
    public static AdvertisingEvent.EStatus getStatusByCode(int code) {
        switch (code) {
            case 0:
                return AdvertisingEvent.EStatus.FREE;
            case 1:
                return AdvertisingEvent.EStatus.OCCUPIED;
            case 3:
                return AdvertisingEvent.EStatus.INITIALIZATION;
        }
        return null;
    }
}
