package io.nwave.pkudinov.registrationdemo.async;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import io.nwave.pkudinov.registrationdemo.bluetooth.BluetoothUtils;
import io.nwave.pkudinov.registrationdemo.bluetooth.ScanTask;
import io.nwave.pkudinov.registrationdemo.server_api.INwaveServer;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchData;
import io.nwave.pkudinov.registrationdemo.server_api.data.SpotSearchResponse;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.ICancelable;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnErrorResponseListener;
import io.nwave.pkudinov.registrationdemo.server_api.interfaces.IOnResponseListener;

/**
 * Created by Pavel Kudinov on 21.06.2018.
 */
public abstract class SpotSearchAsync extends AsyncTask<String, SpotSearchAsync.IActionMark, Boolean> {
    private ScanTask scanTask;
    private INwaveServer iNwaveServer;
    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    private boolean result;
    private List<ICancelable> httpRequestList = new ArrayList<>();
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private boolean anyDeviceFound;
    private int loopNumber = 0;
    private int loopNumberAdvertisingReceived = 0;
    protected String sssId = "";
    protected String[] available_sensors;
    private long startTime;
    private boolean completed;

    protected abstract SpotSearchData.Geo getGeo();
    protected abstract String getStopTime();
    private boolean firstPass;

    @SuppressWarnings("WeakerAccess")
    public static class SensorInfo {
        public int rssi;

        private SensorInfo(SpotSearchData.Advertising_packet event) {
            rssi = event.rssi;
        }
    }

    protected interface IActionMark {}

    protected class ActionHttpResponse implements IActionMark {
        public SpotSearchResponse spotSearchResponse;
    }
    protected class ActionHttpError implements IActionMark {
        public ExecutionException executionException;
    }
    protected class ActionAdvertisingEvents implements IActionMark {
        public List<SensorInfo> sensorInfoList;
        public SpotSearchData.Geo geo;
    }
    protected class ActionRestartScan implements IActionMark {}
    protected class ActionNoSensorsNearby implements IActionMark {}



    public SpotSearchAsync(ScanTask scanTask, INwaveServer iNwaveServer) {
        this.scanTask = scanTask;
        this.iNwaveServer = iNwaveServer;
    }


    private Runnable periodicTask = new Runnable() {
        @Override
        public void run() {
            if (!firstPass || !sssId.equals("")) {
                ++loopNumber;

                List<SpotSearchData.Advertising_packet> eventList = new ArrayList<>();
                List<ScanTask.BleAdvDevice> allAdvertisingEvents = scanTask.getBleAdvDevices();
                for (ScanTask.BleAdvDevice adv : allAdvertisingEvents) {
                    anyDeviceFound = true;

                    byte[] msd = BluetoothUtils.getManufacturerData(adv.scanRecord);
                    if (BluetoothUtils.isNwaveManufacturer_v2(msd)) {
                        SpotSearchData.Advertising_packet event = new SpotSearchData.Advertising_packet();
                        event.msd = BluetoothUtils.bytesToHex(msd);
                        event.rssi = adv.rssi;
                        eventList.add(event);
                    }
                }

                if (eventList.isEmpty()) {

                    if (loopNumber - loopNumberAdvertisingReceived > 3 && !anyDeviceFound) {
                        loopNumberAdvertisingReceived = loopNumber;
                        publishProgress(new ActionRestartScan());
                        scanTask.stopScanning();
                        scanTask.startScanning();
                    } else if (loopNumber - loopNumberAdvertisingReceived > 10) {
                        publishProgress(new ActionNoSensorsNearby());
                        finishTask(false);
                    }
                } else {
                    loopNumberAdvertisingReceived = loopNumber;
                }

                SpotSearchData spotSearchData = new SpotSearchData();
                spotSearchData.advertising_packets = eventList.toArray(new SpotSearchData.Advertising_packet[0]);
                spotSearchData.geo = getGeo();
                spotSearchData.scan_duration = (int) (System.currentTimeMillis() - startTime);
                spotSearchData.stop_time = getStopTime();

                ActionAdvertisingEvents action = new ActionAdvertisingEvents();
                action.geo = spotSearchData.geo;
                action.sensorInfoList = new ArrayList<>(eventList.size());
                for (SpotSearchData.Advertising_packet event : eventList) {
                    action.sensorInfoList.add(new SensorInfo(event));
                }
                publishProgress(action);

                httpRequestList.add(iNwaveServer.spotSearchRequest(spotSearchData, sssId, httpListener, httpErrorListener));
                firstPass = true;
            }
        }
    };
    private IOnResponseListener<SpotSearchResponse> httpListener = new IOnResponseListener<SpotSearchResponse>() {
        @Override
        public void onResponse(SpotSearchResponse data) {
            if (getStatus() != Status.FINISHED && !completed) {
                ActionHttpResponse action = new ActionHttpResponse();
                action.spotSearchResponse = data;
                publishProgress(action);

                switch (data.status) {
                    case COMPLETED:
                        completed = true;
                        sssId = data.sssid;
                        available_sensors = new String[data.available_sensors.length];
                        for (int i = 0; i < available_sensors.length; ++i) {
                            available_sensors[i] = data.available_sensors[i].sensor_id;
                        }
                        finishTask(true);
                        break;
                    case CONTINUE:
                        sssId = data.sssid;
                        break;
                    case TIMEOUT:
                        completed = true;
                        finishTask(false);
                        break;
                }
            }
        }
    };
    private IOnErrorResponseListener httpErrorListener = new IOnErrorResponseListener() {
        @Override
        public void onErrorResponse(ExecutionException error) {
            if (getStatus() != Status.FINISHED) {
                ActionHttpError action = new ActionHttpError();
                action.executionException = error;
                publishProgress(action);
                finishTask(false);
            }
        }
    };

    private void finishTask(boolean sssCompleted) {
        lock.lock();
        result = sssCompleted;
        condition.signal();
        lock.unlock();
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            //sssId = strings[0];
            startTime = System.currentTimeMillis();
            scanTask.startScanning();
            executorService.scheduleAtFixedRate(periodicTask, 500, 500, TimeUnit.MILLISECONDS);

            lock.lock();
            condition.await(60, TimeUnit.SECONDS);
            return result;

        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } finally {
            lock.unlock();
            executorService.shutdown();
            for (ICancelable httpRequest : httpRequestList) {
                httpRequest.cancel();
            }
            scanTask.stopScanning();
        }
    }
}
