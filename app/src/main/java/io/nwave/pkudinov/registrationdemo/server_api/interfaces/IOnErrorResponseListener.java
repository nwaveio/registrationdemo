package io.nwave.pkudinov.registrationdemo.server_api.interfaces;

import java.util.concurrent.ExecutionException;

/**
 * Created by Pavel Kudinov on 20.06.2018.
 */
@FunctionalInterface
public interface IOnErrorResponseListener {
    void onErrorResponse(ExecutionException error);
}
